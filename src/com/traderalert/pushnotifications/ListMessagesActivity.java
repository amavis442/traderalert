package com.traderalert.pushnotifications;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

public class ListMessagesActivity extends Activity {
	Button btnCreateNewMessage;
	ListView listView;
	EmailListAdapter adapter;
	
	AsyncTask<Void,Void,JSONObject> mGetMessagesTask;
	static final String TAG_LIST_MESSAGE = "Traderalert.ListMessagesActivity";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_messages);
		
		IntentFilter filter = new IntentFilter("com.traderalert.updatelist");
        registerReceiver(myReceiver, filter);
		
		listView = (ListView) findViewById(R.id.listMessages);
		adapter = new EmailListAdapter(this);
		adapter.setNotifyOnChange(true);
		// Define a new Adapter
		// First parameter - Context
		// Second parameter - Layout for the row
		// Third parameter - ID of the TextView to which the data is written
		// Forth - the Array of data

		
		
		Log.i(TAG_LIST_MESSAGE,"Voor Aantal in adapter : " + adapter.getCount());
		// Assign adapter to ListView
		listView.setAdapter(adapter); 
		
		//adapter.add("Dit is een test");
		Log.i(TAG_LIST_MESSAGE,"Na Aantal in adapter : " + adapter.getCount());
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			  @Override
			  public void onItemClick(AdapterView<?> parent, View view,
			    int position, long id) {
			    Toast.makeText(getApplicationContext(),
			      "Click ListItem Number " + position, Toast.LENGTH_LONG)
			      .show();
			  }
		}); 
		
		
		btnCreateNewMessage = (Button)findViewById(R.id.btnSendNewMessage);
		
		btnCreateNewMessage.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(), SendAlertActivity.class);
				startActivity(i);
			}
		});

		new GetMessagesTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.list_messages, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		Intent i;
		
		switch (item.getItemId()) {
		case R.id.menu_send_message :
			i = new Intent(getApplicationContext(), SendAlertActivity.class);
			startActivity(i);
			return true;
		case R.id.mnuSettings:
			i = new Intent(this,SettingsActivity.class);
			startActivity(i);
			return true;
		}
		
		return super.onMenuItemSelected(featureId, item);
	}
	
	private void showMessages(JSONObject json) { 
		
		
		if (json != null) {
			int aantal = adapter.getCount();
			Log.i("Listview.voor" ,"Aantal elementen voor update : " + aantal);
			adapter.clear();
			try {
				Log.d(TAG_LIST_MESSAGE,json.toString());
				JSONArray jarr = json.getJSONArray("data");
				int i = jarr.length();
				String msg = "";
				for (int n=0;n < i;n++) {
					JSONObject jk = (JSONObject)jarr.get(n);
					Log.d(TAG_LIST_MESSAGE,"Welke berichten hebben we: "+jk.getString("msg"));
					if (jk !=  null) {
						msg = jk.getString("pair") + " - " +jk.getString("soort") + " - "+ jk.getString("koers");
						MyMessage msgIn = new MyMessage(jk.getString("from"), msg);
						if (msgIn != null) {
							msgIn.setPair(jk.getString("pair"));
							msgIn.setSoort(jk.getString("soort"));
							msgIn.setKoers(jk.getString("koers"));
						
							Log.d(TAG_LIST_MESSAGE,"From is "+msgIn.getUsername());
							adapter.add(msgIn);
						}
					}	
				}
				adapter.notifyDataSetChanged();
				
				listView.refreshDrawableState();
				listView.setVisibility(ListView.INVISIBLE);
				listView.setVisibility(ListView.VISIBLE);
				
				aantal = adapter.getCount();
				MyMessage test = adapter.getItem(0);
				Log.i("Listview.voor" ,"Aantal elementen na update : " + aantal + " " + test.getPair());
				
			} catch (JSONException e) {
				Log.d(TAG_LIST_MESSAGE,"Object is msgIn is leeg :( en we hebben wel data " + e.getMessage());
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Has to be called from another thread
	 * coz of internet traffic.
	 */
	public void reloadMessages() {
		new GetMessagesTask().execute();
	}
	
	private BroadcastReceiver myReceiver = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context context, Intent intent) {
	    	reloadMessages();
	    }
	};
	
	private void displayProgressBar(String s){
		ProgressBar bar=(ProgressBar)findViewById(R.id.progressBar1);
		bar.setVisibility(View.VISIBLE); //View.INVISIBLE, or View.GONE to hide it.
	}
	
	private void updateProgressBar(int progress) {
		ProgressBar bar=(ProgressBar)findViewById(R.id.progressBar1);
		bar.setProgress(progress);
	}
	
	private void dismissProgressBar(){
		ProgressBar bar=(ProgressBar)findViewById(R.id.progressBar1);
		bar.setVisibility(View.INVISIBLE); //View.INVISIBLE, or View.GONE to hide it.
	}
	
	@Override protected void onResume(){
		reloadMessages();
		super.onResume();
	}
	
	private class GetMessagesTask extends AsyncTask<Void, Integer,JSONObject> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displayProgressBar("Downloading...");
		}
		 
		
		@Override
		protected JSONObject doInBackground(Void... params) {
			for (int i = 0; i <= 100; i += 5) {
				try {
					Thread.sleep(50);
		        } catch (InterruptedException e) {
		        	e.printStackTrace();
		        	return null;
		        }
				publishProgress(i);
		    }
			JSONObject json = ServerUtilities.getMessages();
			return json;
		}
		
		
	
		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
			updateProgressBar(values[0]);
		}
		 
		@Override
		protected void onPostExecute(JSONObject result) {
			showMessages(result);
			super.onPostExecute(result);
		    dismissProgressBar();
		}
	}
	
	@Override
	protected void onDestroy(){
		unregisterReceiver(myReceiver);
		super.onDestroy();
	}
}