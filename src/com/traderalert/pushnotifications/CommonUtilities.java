package com.traderalert.pushnotifications;

import android.content.Context;
import android.content.Intent;
 
public final class CommonUtilities {
 
    // give your server registration url here
    static final String SERVER_URL = "http://traderalert.patrickswebsite.nl/index.php";
	//static final String SERVER_URL = "http://192.168.2.2/traderalert/index.php";
	 
    // Google project id
    static final String SENDER_ID = "546206018243";
    static String DEVICE_TOKEN = "";
    
    static Boolean IS_REGISTERED_ON_SERVER = false;
    static Boolean IS_REGISTERED_ON_GCM = false;
    static Boolean IS_USER_LOGGED_IN = false;
    static int USER_ID = 0;
    
    /**
     * Tag used on log messages.
     */
    static final String TAG = "Traderalert";
 
    static final String DISPLAY_MESSAGE_ACTION =
            "com.traderalert.pushnotifications.DISPLAY_MESSAGE";
    static final String REGISTER_ACTION = "com.traderalert.pushnotifications.REGISTER_ACTION";
    
    static final String EXTRA_MESSAGE = "message";
    static final String REG_ID_MESSAGE = "reg_id_message";
 
    /**
     * Notifies UI to display a message.
     * <p>
     * This method is defined in the common helper because it's used both by
     * the UI and the background service.
     *
     * @param context application's context.
     * @param message message to be displayed.
     */
    static void displayMessage(Context context, String message) {
        Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
    
    /* Nadat het apparaat bij GCM is geregistreerd, even een melding sturen en afvangen */
    static void displayRegisterMessage(Context context, String message,String regID) {
        Intent intent = new Intent(REGISTER_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        intent.putExtra(REG_ID_MESSAGE, regID);
        context.sendBroadcast(intent);
    }
    
    /*
    static void resultLoginAttemptMessage(Context context, String message) {
        Intent intent = new Intent(AUTH_ACTION);
        intent.putExtra(EXTRA_MESSAGE, message);
        context.sendBroadcast(intent);
    }
    */
    
}