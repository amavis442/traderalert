package com.traderalert.pushnotifications;

public class MyMessage {
	private String username;
	private String email;
	private String message;
    private int user_id;
    private String pair;
    private String soort;
    private String koers;
    
    
    public MyMessage() {
    }
 
    public MyMessage(String username, String msg) {
        this.username = username;
        this.message = msg;
    }
    
    public MyMessage(String username, String email,String msg) {
        this.username = username;
        this.email = email;
        this.message = msg;
    }
    
    
    public String getUsername() {
    	return username;
    }
    
    public String getMsg() {
    	return message;
    }
    
    public String getEmail() {
    	return email;
    }

    public String getPair() {
    	return pair;
    }
    public String getSoort() {
    	return soort;
    }
    public String getKoers() {
    	return koers;
    }
    
    public int getUserId() {
    	return user_id;
    }
    
    public void setUsername(String username) {
    	this.username = username;
    }
    
    public void setMsg(String msg) {
    	message = msg;
    }
    
    public void setEmail(String email) {
    	this.email = email;
    }

    public void setPair(String pair) {
    	this.pair = pair;
    }

    public void setSoort(String soort) {
    	this.soort = soort;
    }

    public void setKoers(String koers) {
    	this.koers = koers.replace("\n", "");
    	this.koers = koers.replace("\r", "");
    	this.koers = this.koers.replace(".", ",");
    }

    public void setUserId(int user_id) {
    	this.user_id = user_id;
    }
    
}
