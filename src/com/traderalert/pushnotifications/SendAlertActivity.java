package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.TAG;
import static com.traderalert.pushnotifications.CommonUtilities.DEVICE_TOKEN;
import static com.traderalert.pushnotifications.CommonUtilities.USER_ID;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class SendAlertActivity extends Activity {
	Button 		btnSend;
	Spinner 	spPair;
	EditText 	txtKoers;
	RadioButton rdSell;
	RadioButton rdBuy;
	String	optionBuySell;
	String chosenPair;
	String token;
	Integer user_id;
	
	AsyncTask<Void,Void,String> mSendAlertTask;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_alert);
				
		token = DEVICE_TOKEN;
		user_id = USER_ID;
		
		btnSend 	= (Button) findViewById(R.id.btnSend);
		spPair 		= (Spinner) findViewById(R.id.spPair);
		txtKoers 	= (EditText) findViewById(R.id.txtKoers);
		rdSell 		= (RadioButton) findViewById(R.id.rdSell);
		rdBuy 		= (RadioButton) findViewById(R.id.rdBuy);
		
		btnSend.setOnClickListener(new View.OnClickListener() {                           
			public void onClick(View v) {
            	onbtnSendClicked(v);
            }
		});
		
		rdSell.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onrdSellClicked(v);
			}
		});

		rdBuy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onrdBuyClicked(v);
			}
		});
		
		spPair.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int pos,long id) {
				chosenPair = parent.getItemAtPosition(pos).toString();
				/*
				Toast.makeText(parent.getContext(), 
					"OnItemSelectedListener : " + parent.getItemAtPosition(pos).toString(),
					Toast.LENGTH_SHORT).show();
			  	*/
			  }
			 
			  @Override
			  public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			  }
		});
	}

	public String formatSendData() {
		JSONObject json;
		try {
			json = new JSONObject();
			json.put("pair",chosenPair);
			json.put("koers", txtKoers.getText().toString());
			json.put("action", optionBuySell);
			json.put("token", token);
			json.put("user_id", user_id);
			return json.toString();
		} catch (JSONException e) {
			Log.d(TAG, "SendAlert JSON Error " + e.getMessage());
		}
		return null;
	}
	
	public void NotifyUserMsgHasBeenSend() {
		Toast.makeText(this, "Message has been send", Toast.LENGTH_SHORT).show();
	}
	
	public void onbtnSendClicked(View v) {
        mSendAlertTask = new AsyncTask<Void, Void, String>() {
    		@Override
        	protected String doInBackground(Void...params) {
    			Log.v(TAG, "Klaarmaken om Json data te versturen");
    			String data = formatSendData();
    			Log.v(TAG, "Json data om te versturen : "+data);
    			
    			String r = Webservice.postJSON("/message/receivemsg", data);
    			Log.v(TAG, "Received data : "+r);
    			
    			return r;
    		}
    		
    		protected void onPostExecute(String msg) {
    			 NotifyUserMsgHasBeenSend();
    			
    			mSendAlertTask = null;
    		}
        };
        
        mSendAlertTask.execute();
 	}
	
	
	public void onrdSellClicked(View view){
		optionBuySell = "Sell";
	}

	public void onrdBuyClicked(View view){
		optionBuySell = "Buy";
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.send_alert, menu);
		return true;
	}

}
