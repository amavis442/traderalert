package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.ListMessagesActivity.TAG_LIST_MESSAGE;
import java.util.ArrayList;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class EmailListAdapter extends ArrayAdapter<MyMessage> {
	private final Context context;
	//private final List<String> values;
 
	private ArrayList<MyMessage> list_msg;
	
	public EmailListAdapter(Context context) {
		super(context, R.layout.list_email_view_item);
		list_msg = new ArrayList<MyMessage>();
		this.context = context;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.list_email_view_item, parent, false);

		TextView lblUsername = (TextView) rowView.findViewById(R.id.lblFrom);
		TextView lblPair	= (TextView) rowView.findViewById(R.id.lblPair);
		TextView lblSoort	= (TextView) rowView.findViewById(R.id.lblSoort);
		TextView lblKoers	= (TextView) rowView.findViewById(R.id.lblKoers);

		//Button btnDeleteImage = (Button) rowView.findViewById(R.id.btnDeleteItem);
		
		MyMessage msg = list_msg.get(position);
		if (msg != null) {
			Log.d(TAG_LIST_MESSAGE,msg.getUsername());
		
			if (lblUsername != null) {
				lblUsername.setText(msg.getUsername());
			} else {
				Log.d(TAG_LIST_MESSAGE,"lblUsername:: TextView kan geen setText uitvoeren :( " + msg.getUsername());
			}

			if (lblPair != null) {
				lblPair.setText(msg.getPair());
			} else {
				Log.d(TAG_LIST_MESSAGE,"Pair:: TextView kan geen setText uitvoeren :( " + msg.getPair());
			}
			if (lblSoort != null) {
				lblSoort.setText(msg.getSoort());
			} else {
				Log.d(TAG_LIST_MESSAGE,"Soort:: TextView kan geen setText uitvoeren :( " + msg.getSoort() );
			}
			if (lblKoers != null) {
				lblKoers.setText(msg.getKoers());
			} else {
				Log.d(TAG_LIST_MESSAGE,"Koers:: TextView kan geen setText uitvoeren :( " + msg.getKoers());
			}
		}
		return rowView;
	}
	
	@Override
	public void clear() {
		list_msg.clear();
		super.clear();
	}
	
	@Override
	public void add(MyMessage msg) {
		if (msg != null) {
			list_msg.add(msg);
			super.add(msg);
		} else {
			Log.i(TAG_LIST_MESSAGE,"Msg object is leeg :(");
		}
	}
	
	/*
	public void setNotifyOnChange(Boolean b) {
		super.setNotifyOnChange(b);
	}*/
	
}
