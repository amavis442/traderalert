package com.traderalert.pushnotifications;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;


public class SettingsActivity extends PreferenceActivity {
	EditTextPreference txtName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	
	
		txtName = (EditTextPreference) findPreference(getString(R.string.pref_account_name_key));
		//txtName.getEditText().setKeyListener(input);
		populateFields();
	}
	
	private void populateFields() {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String prefName = getString(R.string.pref_account_name_key);
		String prefPassword = getString(R.string.pref_account_password_key);
		String prefSound = getString(R.string.pref_sound_key);
		
		String name = prefs.getString(prefName,"Enter your name");
		String password = prefs.getString(prefPassword, "temp");
		String sound = prefs.getString(prefSound, "");
		//txtName.setText(name);
		
		Log.d("SETTINGS",sound);
	}
	
	
	

}
