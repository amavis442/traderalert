package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.TAG;
import static com.traderalert.pushnotifications.CommonUtilities.IS_USER_LOGGED_IN;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

/**
 * 
 * @author patrick
 *
 */
public class UserAuthTask {
	String email;
	String password;
	String token;
	Boolean validUser = false;

	// Asyntask
    AsyncTask<Void, Void, JSONObject> mUserAuthTask;
    
	/**
	 * T
	 * @param email
	 * @param password
	 * @param token
	 */
	public UserAuthTask (String email, String password, String token) {
		this.email = email;
		this.password = password;
		this.token = token;
	}

	/**
	 * 
	 * @return
	 */
	private String formatJson() {
		JSONObject json;
		Log.v(TAG,"Hij komt hier om de gebruiker te authenticeren");

		json = new JSONObject();
		try {
			json.put("email", email);
			json.put("pw", password);
			json.put("token", token);
			Log.v(TAG,"JSON data : " + json.toString());
			return  json.toString();
		} catch(JSONException e) {
			Log.v(TAG,"Auth:: JSON troubles. " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * 
	 * @param loginActivity
	 */
	public void authenticateUser(LoginActivity loginActivity) {
		mUserAuthTask = new AuthTask(loginActivity).execute();
	}
	
	/**
	 * 
	 * @return
	 */
	public String authenticate() {
		String jsonStr = formatJson();
		return Webservice.postJSON("/user/appLogin",jsonStr);
	}
	
	/**
	 * 
	 * @author patrick
	 *
	 */
	class AuthTask extends AsyncTask<Void, Void, JSONObject> {
		LoginActivity loginActivity;
		
		/**
		 * 
		 * @param loginActivity
		 */
		public AuthTask(LoginActivity loginActivity) {
			this.loginActivity = loginActivity;
		}
		
		@Override
		/**
		 * 
		 */
		protected JSONObject doInBackground(Void...params) {
			String result = authenticate();
			Log.v(TAG,"Output:: " + result);
			if (result != null) {
				try {
					JSONObject j = new JSONObject(result);
					return j;
				} catch(JSONException e) {
					Log.v(TAG,"doInBackground Auth:: JSON troubles:: " + e.getMessage());
					return null;
				}
			}
			return null;
		}
		
		@Override
		/**
		 * 
		 */
		protected void onPostExecute(JSONObject result) {
			try {
				if (result != null && result.getBoolean("status")) {
					IS_USER_LOGGED_IN = true;
					loginActivity.loggedInNextScreen(result.getInt("user_id"));
				} else {
					IS_USER_LOGGED_IN = false;
				}
			} catch(Exception e) {
				IS_USER_LOGGED_IN = false;
			}
		}
	}
}
