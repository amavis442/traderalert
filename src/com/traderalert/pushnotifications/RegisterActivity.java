package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.EXTRA_MESSAGE;
import static com.traderalert.pushnotifications.CommonUtilities.IS_REGISTERED_ON_SERVER;
import static com.traderalert.pushnotifications.CommonUtilities.REGISTER_ACTION;
import static com.traderalert.pushnotifications.CommonUtilities.REG_ID_MESSAGE;
import static com.traderalert.pushnotifications.CommonUtilities.SENDER_ID;
import static com.traderalert.pushnotifications.CommonUtilities.SERVER_URL;
import static com.traderalert.pushnotifications.CommonUtilities.TAG;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;
 
public class RegisterActivity extends Activity {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
 
    // Internet detector
    ConnectionDetector cd;
 
    // UI elements
    EditText txtName;
    EditText txtEmail;
 
    // Register button
    Button btnRegister;
    Button btnUnregister;
	
    public static String name;
	public static String email;
	
    AsyncTask<Void,Void,String> mRegisterTask;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
 
        cd = new ConnectionDetector(getApplicationContext());
 
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(RegisterActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
 
        // Check if GCM configuration is set
        if (SERVER_URL == null || SENDER_ID == null || SERVER_URL.length() == 0 || SENDER_ID.length() == 0) {
            // GCM sender id / server url is missing
            alert.showAlertDialog(RegisterActivity.this, "Configuration Error!",
                    "Please set your Server URL and GCM Sender ID", false);
            // stop executing code by return
             return;
        }
        
        /* Nadat we geregistreerd zijn krijgen we van gcm een melding terug met een regid
         * en wordt GCMIntentService.onRegistered opgeroepen.
         * Deze stuurt vervolgens een bericht de wereld in die we met onderstaande code
         * afvangen en het bericht laten zien.
         */
        registerReceiver(mHandleMessageReceiver, new IntentFilter(REGISTER_ACTION));
		
        
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnUnregister = (Button) findViewById(R.id.btnUnregister);
        /*
         * Click event on Register button
         * */
        btnRegister.setOnClickListener(new View.OnClickListener() {
             @Override
            public void onClick(View v) {
            	onbtnRegisterClicked(v);
            }
        });
        
        btnUnregister.setOnClickListener(new View.OnClickListener() {
            @Override
           public void onClick(View v) {
            	onbtnUnregisterClicked(v);
           }
       });
        Log.d(TAG,"RegisterActivity onCreate done.");
    }
 
    private String formatJSON(String name,String email, String token) {
		JSONObject json;
		Log.v(TAG,"Hij komt hier om de gebruiker te authenticeren");

		json = new JSONObject();
		try {
			json.put("name", name);
			json.put("email", email);
			json.put("token", token);
			Log.v(TAG,"JSON data : " + json.toString());
			return  json.toString();
		} catch(JSONException e) {
			Log.v(TAG,"Auth:: JSON troubles. " + e.getMessage());
		}
		return null;
    }
    
    private void NotifyUser() {
    	Toast.makeText(this, "Registration has been send and awaiting for aprovement.", Toast.LENGTH_SHORT).show();
    	GCMRegistrar.setRegisteredOnServer(this, true);
    	if (mRegisterTask != null){
    		mRegisterTask = null;
    	}
    	backToLoginScreen();
    }
    
    private void backToLoginScreen() {
    	Intent i = new Intent(getApplicationContext(), LoginActivity.class);
    	/* i.putExtra("token", regId);
    	i.putExtra("email", email); */
    	startActivity(i);
    	finish();
    }
    
    public void resultRegistration(String result) {
    	if (result.contains("status")) {
    		try {
    			JSONObject json = new JSONObject(result);
    			if (json.getBoolean("status")) {
    				/* Geregistreerd en even laten weten */
    				NotifyUser();
    			}
    		} catch (JSONException e) {
			
    		}
    	}
    }
    
    public void onbtnRegisterClicked(View v) {
    	Toast.makeText(this, "Ready to register.", Toast.LENGTH_SHORT).show();
    	
    	name = txtName.getText().toString();
    	email = txtEmail.getText().toString();
    	
    	
    	// Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
    	
        // Make sure the manifest was properly set - comment out this line
        // while developing the app, then uncomment it when it's ready.
        GCMRegistrar.checkManifest(this);
    	                
        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
        
        // Check if regid already presents
        if (regId.equals("")) {
            // Registration is not present, register now with GCM
            GCMRegistrar.register(this, SENDER_ID);
            saveSettings();
            
        } else {
            // Device is already registered on GCM
            if (GCMRegistrar.isRegisteredOnServer(this)) {
            	 Toast.makeText(getApplicationContext(), "Already registered with GCM", Toast.LENGTH_LONG).show();
            	 backToLoginScreen();
            } else {
            	Log.d(TAG,"Niet geregistreerd, maar zou dat wel moeten zijn :(");
            }
        }
    }
    
    public void onbtnUnregisterClicked(View v) {
    	// Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        
        // Get GCM registration id
        final String regId = GCMRegistrar.getRegistrationId(this);
        
        // Check if regid already presents
        if (!(regId.equals(""))) {
        	GCMRegistrar.unregister(this);
        	//GCMRegistrar.setRegisteredOnServer(this, false);
        } else {
        	Toast.makeText(this, "You are not yet registeren.", Toast.LENGTH_SHORT).show();
        	return;
        }
    }
    
    /**
     * Initial preferences settings
     */
    private void saveSettings() {
    	SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    	Editor edit = prefs.edit();
    	edit.putString(getString(R.string.pref_account_name_key), name);
    	edit.putString(getString(R.string.pref_account_password_key),"temp");
    	edit.putString(getString(R.string.pref_sound_key), "startrek.mp3");
    	edit.commit();
    }	
    
    
	/**
	 * Receiving push messages
	 * */
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String newMessage = intent.getExtras().getString(EXTRA_MESSAGE);
			newMessage += "\n" + intent.getExtras().getString(REG_ID_MESSAGE);
			// Waking up mobile if it is sleeping
			//WakeLocker.acquire(getApplicationContext());
			
			/**
			 * Take appropriate action on this message
			 * depending upon your app requirement
			 * For now i am just displaying it on the screen
			 * */
			
			// Showing received message
			//lblMessage.append(newMessage + "\n");			
			Toast.makeText(getApplicationContext(), "New Message: " + newMessage, Toast.LENGTH_LONG).show();
			
			if (IS_REGISTERED_ON_SERVER) {
				/* Back to login screen */
				Intent i = new Intent(getApplicationContext(), LoginActivity.class);
		    	i.putExtra("email", email);
		    	startActivity(i);
			}
			
			// Releasing wake lock
			//WakeLocker.release();
		}
	};
}