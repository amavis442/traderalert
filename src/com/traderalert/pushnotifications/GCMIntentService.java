package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.IS_REGISTERED_ON_GCM;
import static com.traderalert.pushnotifications.CommonUtilities.IS_REGISTERED_ON_SERVER;
import static com.traderalert.pushnotifications.CommonUtilities.SENDER_ID;
import static com.traderalert.pushnotifications.CommonUtilities.displayMessage;
import static com.traderalert.pushnotifications.CommonUtilities.displayRegisterMessage;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {
 
    private static final String TAG = "GCMIntentService";
    private static Boolean isRegistered = true;
    
    public GCMIntentService() {
        super(SENDER_ID);
    }
 
    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {
        Log.i(TAG, "Device registered: regId = " + registrationId);
        IS_REGISTERED_ON_GCM = true;
        /* Registreren bij de webserver */
        String result = ServerUtilities.register(context, RegisterActivity.name, RegisterActivity.email, registrationId);
        String msg = "Your device registred with GCM";
        
        try {
        	JSONObject json = new JSONObject(result);
        	if (json.getBoolean("status")) {
        		IS_REGISTERED_ON_SERVER = true;
        	}
        } catch (JSONException e) {
        	msg = "Somthing went wrong with the JSONObject thingy.";
        	Log.d(TAG,"JSON error grrrr....");
        	IS_REGISTERED_ON_SERVER = false;
        }
        
        displayRegisterMessage(context, msg,registrationId);
    }
 
    /**
     * Method called on device unregistred
     * */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");
        displayRegisterMessage(context, getString(R.string.gcm_unregistered),";)");
        //ServerUtilities.unregister(context, registrationId);
    }
 
    /**
     * Method called on Receiving a new message
     * */
    @Override
    protected void onMessage(Context context, Intent intent) {
        Log.i("Messages", "Received message");
               
        if (intent.getExtras().isEmpty()) {
        	Log.d("Messages","Geen intent data beschikbaar :(");
        	return;
        } else {
        	String content = intent.getExtras().toString();
        	Log.d("Messages",content);
        }
        		
        String data = intent.getExtras().getString("data");
    	Log.i("Messages", "Received data : " + data);
        Intent intentListUpdate = new Intent();

        if (data != null && data.length() > 0) {
        	try {
        		JSONObject json = new JSONObject(data);
        		if (json.has("actie")) {
            		if (json.getString("actie").equals("reload")) {
                        intentListUpdate.setAction("com.traderalert.updatelist");
                        sendBroadcast(intentListUpdate); // finally broadcast
                      	generateNotification(context, json);
                        if (json.has("msg")) {
                        	displayMessage(context, json.getString("msg"));
                        }
            		}
            	}		
        	} catch(JSONException e) {
        		Log.d(TAG,e.getMessage());
        		e.printStackTrace();
        	}
        } else {
        	displayMessage(context,"No message received :(");
        	Log.i("Messages","Geen bericht ontvangen of een leeg bericht [" + data + "]");
        }
        //displayMessage(context, message);
    }

    /**
     * Method called on receiving a deleted message
     * */
    @Override
    protected void onDeletedMessages(Context context, int total) {
        Log.i(TAG, "Received deleted messages notification");
        String message = getString(R.string.gcm_deleted, total);
        displayMessage(context, message);
        // notifies user
        generateNotification(context, message);
    }
 
    /**
     * Method called on Error
     * */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        displayMessage(context, getString(R.string.gcm_error, errorId));
    }
 
    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        displayMessage(context, getString(R.string.gcm_recoverable_error,
                errorId));
        return super.onRecoverableError(context, errorId);
    }
 
    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, JSONObject json) {
    	int icon = R.drawable.ic_launcher;
    	long when = System.currentTimeMillis();

    	if (json != null) {
    		try {
    			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    			Notification notification = new Notification(icon, json.getString("ticker"), when);
 
    			//String title = context.getString(R.string.app_name);
 
    			Intent notificationIntent = new Intent(context, ListMessagesActivity.class);
    			// set intent so it does not start a new activity
    			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
    			PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
    			notification.setLatestEventInfo(context, json.getString("title"), json.getString("msg"), intent);
    			notification.flags |= Notification.FLAG_AUTO_CANCEL;
 
    			// Play default notification sound
    			//notification.defaults |= Notification.DEFAULT_SOUND;
    			Uri sounduri = getAlarmUri(context);
    			notification.sound = sounduri;
    			
    			
    			// Vibrate if vibrate is enabled
    			notification.defaults |= Notification.DEFAULT_VIBRATE;
    			notificationManager.notify(0, notification);     
    		} catch (JSONException e) {
    			e.printStackTrace();
    			Log.d(TAG,e.getMessage());
    		}
    	}
    }
 
    /**
     * Issues a notification to inform the user that server has sent a message.
     */
    private static void generateNotification(Context context, String message) {
        int icon = R.drawable.ic_launcher;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
 
        String title = context.getString(R.string.app_name);
 
        Intent notificationIntent = new Intent(context, ListMessagesActivity.class);
        // set intent so it does not start a new activity
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
        notification.setLatestEventInfo(context, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
 
        // Play default notification sound
        //notification.defaults |= Notification.DEFAULT_SOUND;
 		Uri sounduri = getAlarmUri(context);
		notification.sound = sounduri;

		
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(0, notification);     
    }
    
    //Get an alarm sound. Try for an alarm. If none set, try notification,
    //Otherwise, ringtone.
    private static Uri getAlarmUri(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		
		String prefSound = context.getString(R.string.pref_sound_key);
		String sound = prefs.getString(prefSound,"-");
		Uri alert;
		
		if (!sound.equals("-")) {
			alert = Uri.parse(sound);
		} else {		
			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
		}
		if (alert == null) {
    		alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    		if (alert == null) {
    			alert = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
       		}
    	}
    	return alert;
    }
}