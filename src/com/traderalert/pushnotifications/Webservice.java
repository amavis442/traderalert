package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.SERVER_URL;
import static com.traderalert.pushnotifications.CommonUtilities.TAG;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.client.ClientProtocolException;

import android.util.Log;

/**
 * 
 * @author patrick
 *
 */
public class Webservice {
	/**
	 * 
	 * @param instream
	 * @return
	 * @throws IOException
	 */
	private static String readStream(InputStream instream) throws IOException{
		try {
			InputStream in 		= new BufferedInputStream(instream);
			BufferedReader rd 	= new BufferedReader(new InputStreamReader(in));
			String line;
			StringBuffer response = new StringBuffer(); 
			while((line = rd.readLine()) != null) {
				Log.v(TAG,"Data server : " + line);
				response.append(line);
				response.append('\r');
			}
			rd.close();
			return response.toString();
		} catch (IOException e) {
			throw e;
		}
	}
	
	/**
	 * Sends JSON formated data to the webserver and retrieves the output
	 * from the server. This output should also be json formatted but can also be
	 * a string if something went wrong.
	 * 
	 * @param targetUrl
	 * @param data
	 * @return
	 */
	public static String postJSON(String targetUrl,String data) {
		URL url;
		HttpURLConnection connection = null;  
		String serverUrl = SERVER_URL + targetUrl;

		Log.v(TAG,"Hij komt hier om de gebruiker te authenticeren");
		
			
		try {
			//Create connection
			url = new URL(serverUrl);
			connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", 
					"application/x-www-form-urlencoded");
	
			connection.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
			connection.setRequestProperty("Content-Language", "en-US");  
		
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);

			Log.v(TAG,"Klaar om de data te verzenden naar de server " + serverUrl + ". met als jsonstring " + data);
	
			//Send request
			DataOutputStream wr = new DataOutputStream(
							connection.getOutputStream ());
			wr.writeBytes (data);
			wr.flush ();
			wr.close ();
			Log.v(TAG,"Klaar om de data te onvangen");
			
			try {
			    return readStream(connection.getInputStream());
			} catch (IOException e) {
				try {
					String error = readStream(connection.getErrorStream());
					Log.v(TAG,"Gave me an error page :( " + error);
					return null;
				} finally {
					Log.v(TAG,"Crap server");
					connection.disconnect();
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			Log.v(TAG,"UnsupportedEncodingException:: " + e.getMessage());
			return null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			Log.v(TAG,"ClientProtocolException::" + e.getMessage());
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			Log.v(TAG,"IOException:: " + e.getMessage());
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			Log.v(TAG,"Exception:: " + e.getMessage());
			return null;
		} finally {
			if(connection != null) {
				connection.disconnect(); 
			}
		}
	}
}
