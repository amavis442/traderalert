package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.SERVER_URL;
import static com.traderalert.pushnotifications.CommonUtilities.TAG;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.google.android.gcm.GCMRegistrar;
 
public final class ServerUtilities {
    private static final int MAX_ATTEMPTS = 5;
    private static final int BACKOFF_MILLI_SECONDS = 2000;
    private static final Random random = new Random();

    static String jsonRegData(String name, String email, String regId) {
        JSONObject json;
        try {
        	json = new JSONObject();
        	json.put("name", name);
        	json.put("email", email);
        	json.put("token", regId);
        } catch (JSONException e) { 
        	Log.i(TAG, "registering JSON Error " + e.getMessage());
        	json = null;
        	return null;
        }
        return json.toString();
    }
    
    /**
     * Register this account/device pair within the server.
     *
     */
    static String register(final Context context, String name, String email, final String regId) {
        Log.i(TAG, "registering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/user/appRegister";
        /* 
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        params.put("name", name);
        params.put("email", email);
 		*/
        String data = jsonRegData(name,email,regId);
        
        long backoff = BACKOFF_MILLI_SECONDS + random.nextInt(1000);
        // Once GCM returns a registration id, we need to register on our server
        // As the server might be down, we will retry it a couple
        // times.
       
        if (data != null) {
     		//displayMessage(context, context.getString(R.string.server_registering, i, MAX_ATTEMPTS));
   			String result = Webservice.postJSON("/user/appRegister",data);
            Log.d(TAG,result);    
   			GCMRegistrar.setRegisteredOnServer(context, true);
   			//String message = context.getString(R.string.server_registered);
   			//CommonUtilities.displayMessage(context, message);
   			return result;
        }
        return null;
    }
    
    /**
     * Unregister this account/device pair within the server.
     */
    static void unregister(final Context context, final String regId) {
        Log.i(TAG, "unregistering device (regId = " + regId + ")");
        String serverUrl = SERVER_URL + "/unregister";
        Map<String, String> params = new HashMap<String, String>();
        params.put("regId", regId);
        try {
            post(serverUrl, params);
            GCMRegistrar.setRegisteredOnServer(context, false);
            String message = context.getString(R.string.server_unregistered);
            CommonUtilities.displayMessage(context, message);
        } catch (IOException e) {
            // At this point the device is unregistered from GCM, but still
            // registered in the server.
            // We could try to unregister again, but it is not necessary:
            // if the server tries to send a message to the device, it will get
            // a "NotRegistered" error message and should unregister the device.
            String message = context.getString(R.string.server_unregister_error,
                    e.getMessage());
            CommonUtilities.displayMessage(context, message);
        }
    }
 
    static JSONObject getMessages() {
    	try {
    		JSONObject json = new JSONObject();
    		json.put("limit","10");
    		String messages = Webservice.postJSON("/message/appGetmessages",json.toString());
    		if (messages != null) {
    			Log.d(TAG,messages);
    		}
    		return new JSONObject(messages);
    	} catch (JSONException e) {
    		Log.d(TAG,"Getmessages heeft problemen: " + e.getMessage());
    		e.printStackTrace();
    	}
    	return null;
    }
    
    
    static HttpURLConnection auth(String email, String password, final String regId) {
    	HttpURLConnection conn;
    	JSONObject json;
    	String jsondata = "";
    	
    	Log.v(TAG,"Hij komt hier om de gebruiker te authenticeren");
    	String serverUrl = SERVER_URL + "/user/appLogin";
    	//Map<String, String> params = new HashMap<String, String>();
        
    	//params.put("regId", regId);
        //params.put("email", email);
        //params.put("pw", password);
    	json = new JSONObject();
        try {
        	//json = new JSONObject();
        	json.put("email", email);
        	json.put("pw", password);
        	json.put("token", regId);
        	jsondata = json.toString();
        	Log.v(TAG,"JSON data : " + jsondata);
        } catch(JSONException e) {
        	Log.v(TAG,"Auth:: JSON troubles. " + e.getMessage());
        }
        
        try {
        	conn = (HttpURLConnection) excuteJSONPost(serverUrl,json);
        	return conn;
       	} catch (IOException e) {
            Log.v(TAG,"Auth:: Server troubles. " + e.getMessage());
            //CommonUtilities.resultLoginAttemptMessage(context, e.getMessage());
            return null;
        } finally {
        	Log.v(TAG,"Auth:: Server finally troubles. ");
        }
        /*
    	try {
    		result = post(serverUrl,params);
    	} catch (IOException e) {
            result = e.getMessage();
    		//CommonUtilities.resultLoginAttemptMessage(context, e.getMessage());
        }
        */
    }
    
    public static HttpURLConnection excuteJSONPost(String targetURL, JSONObject json) 
    		throws IOException { 
    	URL url;
    	HttpURLConnection connection = null;  
    	String jsondata = (String) json.toString();
      
    	try {
    		//Create connection
    		url = new URL(targetURL);
    		connection = (HttpURLConnection)url.openConnection();
    		connection.setRequestMethod("POST");
    		connection.setRequestProperty("Content-Type", 
    				"application/x-www-form-urlencoded");
  		
    		connection.setRequestProperty("Content-Length", "" + Integer.toString(jsondata.getBytes().length));
    		connection.setRequestProperty("Content-Language", "en-US");  
  			
    		connection.setUseCaches(false);
    		connection.setDoInput(true);
    		connection.setDoOutput(true);

    		Log.v(TAG,"Klaar om de data te verzenden naar de server "+targetURL+ ". met als jsonstring " + jsondata);
    		
    		//Send request
    		DataOutputStream wr = new DataOutputStream(
        							connection.getOutputStream ());
    		wr.writeBytes (jsondata);
    		wr.flush ();
    		wr.close ();

    		return connection;
    		/*
    		Log.v(TAG,"Klaar om de data te onvangen");
    		//Get Response	
    		InputStream is		= connection.getInputStream();
    		if (is != null) {
    			BufferedReader rd 	= new BufferedReader(new InputStreamReader(is));
    			String line;
    			StringBuffer response = new StringBuffer(); 
    			while((line = rd.readLine()) != null) {
    				Log.v(TAG,"Data server : " + line);

    				response.append(line);
    				response.append('\r');
    			}
    			rd.close();
        		return response.toString();
    		} else {
    			return "Shit shit shit"; 
    		}
    		*/
   		} catch (UnsupportedEncodingException e) {
   			e.printStackTrace();
   			Log.v(TAG,"UnsupportedEncodingException:: " + e.getMessage());
    	    return null;
   		} catch (ClientProtocolException e) {
   			e.printStackTrace();
   			Log.v(TAG,"ClientProtocolException::" + e.getMessage());
   			return null;
   		} catch (IOException e) {
   			e.printStackTrace();
   			Log.v(TAG,"IOException:: " + e.getMessage());
   			return null;
   		} catch (Exception e) {
   			e.printStackTrace();
   			Log.v(TAG,"Exception:: " + e.getMessage());
   			return null;
   		} finally {
    	  if(connection != null) {
    		  connection.disconnect(); 
    	  }
      }
    }
    
    
    /**
     * Issue a POST request to the server.
     *
     * @param endpoint POST address.
     * @param params request parameters.
     *
     * @throws IOException propagated from POST.
     */
    private static String post(String endpoint, Map<String, String> params)
            throws IOException {   
 
        URL url;
        try {
            url = new URL(endpoint);
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("invalid url: " + endpoint);
        }
        StringBuilder bodyBuilder = new StringBuilder();
        Iterator<Entry<String, String>> iterator = params.entrySet().iterator();
        // constructs the POST body using the parameters
        while (iterator.hasNext()) {
            Entry<String, String> param = iterator.next();
            bodyBuilder.append(param.getKey()).append('=')
                    .append(param.getValue());
            if (iterator.hasNext()) {
                bodyBuilder.append('&');
            }
        }
        String body = bodyBuilder.toString();
        Log.v(TAG, "Posting '" + body + "' to " + url);
        byte[] bytes = body.getBytes();
        HttpURLConnection conn = null;
        try {
            Log.e("URL", "> " + url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setFixedLengthStreamingMode(bytes.length);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded;charset=UTF-8");
            // post the request
            OutputStream out = conn.getOutputStream();
            out.write(bytes);
            out.close();
            // handle the response
            int status = conn.getResponseCode();
            if (status != 200) {
              throw new IOException("Post failed with error code " + status);
            } else {
            	/* Lezen van de input */
            	InputStream is = conn.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer(); 
                while((line = rd.readLine()) != null) {
                  response.append(line);
                  response.append('\r');
                }
                rd.close();
                return response.toString();
            }
        } finally {
            if (conn != null) {
                conn.disconnect();
           }
        }
    }
}