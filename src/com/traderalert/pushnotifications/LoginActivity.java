package com.traderalert.pushnotifications;

import static com.traderalert.pushnotifications.CommonUtilities.DEVICE_TOKEN;
import static com.traderalert.pushnotifications.CommonUtilities.IS_USER_LOGGED_IN;
import static com.traderalert.pushnotifications.CommonUtilities.TAG;
import static com.traderalert.pushnotifications.CommonUtilities.USER_ID;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gcm.GCMRegistrar;

public class LoginActivity extends Activity {
    // alert dialog manager
    AlertDialogManager alert = new AlertDialogManager();
   
	// Internet detector
    ConnectionDetector cd;
    
    // UI elements
    EditText txtLoginEmail;
    EditText txtLoginPassword;
 
    // Register button
    Button btnLogin;
    TextView lblRegistreren;
    
    String regId;
    String email;
    String password;
    UserAuthTask authtask;
    private ProgressDialog progressDialog;
  
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	setContentView(R.layout.activity_login);
    	    	
    	cd = new ConnectionDetector(getApplicationContext());
    	
        // Check if Internet present
        if (!cd.isConnectingToInternet()) {
            // Internet Connection is not present
            alert.showAlertDialog(LoginActivity.this,
                    "Internet Connection Error",
                    "Please connect to working Internet connection", false);
            // stop executing code by return
            return;
        }
        
        txtLoginEmail 		= (EditText) findViewById(R.id.txtLoginEmail);
    	txtLoginPassword 	= (EditText) findViewById(R.id.txtLoginWachtwoord);
    	btnLogin 			= (Button) findViewById(R.id.btnSend);
          
        btnLogin.setOnClickListener(new View.OnClickListener() {                           
            public void onClick(View v) {
            	onbtnLoginClicked(v);
            }
        });
         
        
        lblRegistreren =  (TextView) findViewById(R.id.lblRegistreren);
        lblRegistreren.setOnClickListener(new View.OnClickListener() {                           
            public void onClick(View arg0) {
            	onlblRegisterClicked(arg0);
            }
        });
        
        Intent i = getIntent();
        if (i != null) {
        	//name = i.getStringExtra("name");
        	email = i.getStringExtra("email");
        	txtLoginEmail.setText(email);
        }
    }
    
    /* Check if we are indeed registered with gcm and
     * if the user has been authenticated.
     */
    public void onbtnLoginClicked(View view) {
        // Read EditText data
        email = txtLoginEmail.getText().toString();
        password = txtLoginPassword.getText().toString();
        
        if (email.equals("")) {
        	new AlertDialog.Builder(this)
        	.setTitle("Error")
        	.setMessage(R.string.login_error_no_login)
        	.setCancelable(false)
        	.setPositiveButton("OK", new DialogInterface.OnClickListener() {

        	            public void onClick(DialogInterface dialog, int which) {


        	            }
        	        })
        	    .show();
        	return;
        }
        
        if (password.equals("")) {
        	new AlertDialog.Builder(this)
        	.setTitle("Error")
        	.setMessage(R.string.login_error_no_password)
        	.setCancelable(false)
        	.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        	            public void onClick(DialogInterface dialog, int which) {

        	            }
        	        })
        	    .show();
        	return;
        }
       
        Log.v(TAG  , "We hebben een email/password ("+email+"/"+password+")");
        
        //registerReceiver(mHandleMessageReceiver, new IntentFilter(AUTH_ACTION));
        
        // Make sure the device has the proper dependencies.
        GCMRegistrar.checkDevice(this);
        
        Boolean isRegistered = GCMRegistrar.isRegistered(this);
        if (!isRegistered) {
            alert.showAlertDialog(LoginActivity.this,
                    "Device error",
                    "Device is not yet registerd. Please first register before using app.", false);
        	Log.v(TAG,"Registratie id van GCM is  " + regId);
        	DEVICE_TOKEN = regId;
        	return; /* Not registered so we can not do anything */
        } else {
        	// Get GCM registration id
        	regId = GCMRegistrar.getRegistrationId(this);
        	Log.v(TAG,"Registratie id van GCM is  " + regId);
                
        	// Check if regid already presents else register
        	if (regId.equals("")) {
        		new AlertDialog.Builder(this)
        		.setTitle("Error")
        		.setMessage(R.string.login_error_no_registration)
        		.setCancelable(false)
        		.setPositiveButton(
        				"OK", new DialogInterface.OnClickListener() {

        					public void onClick(DialogInterface dialog, int which) {


        					}
        				})
        				.show();
        		return;
        	}
        	DEVICE_TOKEN = regId;
        	
        	new mLoginTask().execute();
        	
        	//authtask =  new UserAuthTask(email,password,regId);
        	//authtask.authenticateUser(this);
        }
    }
    
    public void loggedInNextScreen(Integer user_id) {
    	Intent i = new Intent(getApplicationContext(), ListMessagesActivity.class);
    	USER_ID = user_id;
    	startActivity(i);
    	finish();
    }
    
    /* Not registered yet? */
    public void onlblRegisterClicked(View view) {
    	 Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
         startActivity(i);
         Log.d(TAG,"Klaar met het inlogscherm. We moeten gaan registreren.");
         //finish(); Je mag gewoon terug naar het inlogscherm
    }
    
      
	private String AuthJson() {
		JSONObject json;
		Log.v(TAG,"Hij komt hier om de gebruiker te authenticeren");

		json = new JSONObject();
		try {
			json.put("email", email);
			json.put("pw", password);
			json.put("token", regId);
			Log.v(TAG,"JSON data : " + json.toString());
			return  json.toString();
		} catch(JSONException e) {
			Log.v(TAG,"Auth:: JSON troubles. " + e.getMessage());
		}
		return null;
	}
    
    private void showInvalidUserPopup(){
    	new AlertDialog.Builder(this)
		.setTitle("Error")
		.setMessage(R.string.login_invalid_user)
		.setCancelable(false)
		.setPositiveButton(
				"OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

					}
				})
		.show();
    }
	
    private void displayProgressDialog() {
    	progressDialog = ProgressDialog.show(LoginActivity.this, "", "Authenticating");
    }
    
    private void hideProgressDialog() {
    	progressDialog.dismiss();
    }
    
    private class mLoginTask extends AsyncTask<Void,Integer,String> {
    	
    	@Override
		protected void onPreExecute() {
			super.onPreExecute();
			displayProgressDialog();
    	}
		 
		
		@Override
		protected String doInBackground(Void... params) {
			String jsonStr = AuthJson();
			String result = Webservice.postJSON("/user/appLogin",jsonStr);
			return result;
		}
		 
		@Override
		protected void onPostExecute(String result) {
			hideProgressDialog();
			
			try {
				JSONObject json  = new JSONObject(result);
				if (json != null && json.getBoolean("status")) {
					IS_USER_LOGGED_IN = true;
					loggedInNextScreen(json.getInt("user_id"));
				} else {
					showInvalidUserPopup();
					IS_USER_LOGGED_IN = false;
				}
			} catch (JSONException e) { 
				Log.v(TAG,e.getMessage());
				IS_USER_LOGGED_IN = false;
				e.printStackTrace();
			} catch(Exception e) {
				IS_USER_LOGGED_IN = false;
			}
			super.onPostExecute(result);
		}
    }
    
    
    @Override
    protected void onDestroy() {
    	if (authtask != null) {
    		authtask.mUserAuthTask.cancel(true);
    	}
    	Log.d(TAG,"Inlogscherm :: onDestroy");
    	super.onDestroy();
    }
    
}
